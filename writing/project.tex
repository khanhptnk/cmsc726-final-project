\documentclass[fleqn]{article}
\twocolumn
\usepackage{scribe}
\setlength{\columnsep}{1cm}
\date{}  % no need to include a date, will save you some space

\usepackage[round]{natbib}
\usepackage{chngpage}
\usepackage{stmaryrd}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{array}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{lscape}
\usepackage{multirow}
\usepackage{parskip}
\usepackage{tabto}
\usepackage{mathtools}
\usepackage{caption}
\usepackage{makecell}
\usepackage{subcaption}

\newenvironment{itemizesquish}{\begin{list}{\labelitemi}{\setlength{\parskip}{0.6cm}\setlength{\itemsep}{0em}\setlength{\labelwidth}{2em}\setlength{\leftmargin}{\labelwidth}\addtolength{\leftmargin}{\labelsep}}}{\end{list}}
\newcommand{\ignore}[1]{}
\newcommand{\para}[1]{(#1)}  
\newcommand{\dir}{\text{Dir}}
\newcommand{\mult}{\text{Multinomial}}
\newcommand{\solution}[1]{{\color{Blue}[\textbf{Solution:} #1]}}
\theoremstyle{definition}
\newtheorem{question}{Question}[section]
% \newtheorem{question}{Question}
\newcommand{\xx}{\emph{\textbf{x}}}
\newcommand{\loss}{\mathcal{L}}
\newcommand{\tloss}{\tilde{\loss}}
\newcommand{\data}{\mathcal{D}}
\newcommand{\tdata}{\tilde{\data}}
\newcommand\eqdef{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny def}}}{=}}}
\newcommand{\lgold}{\loss_{\data_g}}
\newcommand{\lpred}{\loss_{\data_p}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% PROJECT INFORMATION HERE
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Learning in the Wild: Prediction under Noisy Features}
\author{Khanh Nguyen \\ UID 114293039}

\begin{document}
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% PROJECT WRITEUP HERE
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

%Very very brief reminder of what your project is about, just enough to spur Hal's memory.
%Two or three sentences.

We address issues arising when gold labels of some features are only available at training time and are replaced by their erroneous predicted labels at testing time. We show that this incurs a gap between the training and testing objectives, which potentially causes the model to overfit. Moreover, we demonstrate that a naive approach such as always using predicted labels for training is not optimal because although doing so bridges the training-testing gap, it may increase the model's bias. We propose a solution for this problem that utilizes both gold and predicted features, and introduce a grid search algorithm to find the optimal mixing rate.   

\section{Methods}

%Describe (roughly half of the writeup) what you did.
%As necessary, mention what data you used.
%I suggest something like the following structure:

\subsection{Problem formulation}

\subsubsection{Problem Description}

%How did you cast your overall problem (in the intro) as a machine learning task.
%Why is this the right formulation?

We consider machine learning problems where the goal is to predict a variable $y$ of interest from an observed feature vector $\xx$, drawn from a feature vector distribution $\data$. A general solution to these problems is to learn a discriminative model $h_{\theta}$, parametrized by $\theta$, to directly approximate the conditional distribution of $y$ given $\xx$:
\begin{equation}
  h_{\theta}(\xx) \approx P(y \mid \xx)
\end{equation}
Concrete predictions can be inferred from these models as follows:
\begin{equation}
  \hat{y} = \arg\max_y h_{\theta}^{y}(\xx)
\end{equation} where $h_{\theta}^{y}$ is the $y$-th component of $h_{\theta}$.

We are motivated by the fact that, in many problems, not all features are equally easy to compute. For example, basic features such as unigrams or bigrams can be easily extracted from any text input whereas higher-level features such as POS tags or parse trees are very difficult to obtain in many domains. 
We consider scenarios when gold (high-quality) labels for some features are only available at training time. These scenarios are typical in many real-world applications, especially when the model is designed to make predictions on continuously streaming data, hence having to humans to manually label each data instance is impractical. To be able to make predictions at testing time, the most common solution is to train classifiers to predict those features. However, predictions of those classifiers could be erroneous and not closely accurate to human labeling. We focus on analyzing how errors from those predicted features propagate and degrade the quality of a downstream model trained solely on gold features.   

\subsubsection{Learning theory formulation}

In this section, we borrow the concepts of risk minimization and representativeness from learning theory to formalize our problem. 

For solving prediction problems, a common objective is to search for a model that minimizes a loss function under the true data distribution. Formally, this objective can be translated as:
\begin{equation} 
  \begin{split}
    \hat{\theta} &= \arg\min_{\theta} \loss_{\data}(\theta) \\
  &\eqdef \arg\min_{\theta} \mathbb{E}_{ (\xx, y) \sim \data} \left[ L(y, h_{\theta}(\xx))\right]
\end{split}
\end{equation} where $\data$ is the feature distribution, $L$ is a function that takes a true label and a probability distribution as input and returns a non-negative scalar (e.g. squared loss, logistic loss, hinge loss).  

In practice, most of the time, $\data$ is unknown and is approximated by a training sample $S$. Hence, the objective becomes minimizing an empirical loss over $S$:
\begin{equation} 
  \begin{split}
    \hat{\theta}_{S} &= \arg\min_{\theta} \loss_{S}(\theta) \\
    &\eqdef \arg\min_{\theta} \frac{1}{|S|} \sum_{(\xx, y) \in S} L(y, h_{\theta}(\xx))
\end{split}
\end{equation} 


This objective is widely known as \emph{empirical risk minimization} (ERM). When the size of $S$ is too small or $S$ is sampled with bias, the empirical loss deviates from the true loss and causes the model to overfit \footnote{There are various definitions of overfitting. We adopt the definition by \cite{shalev2014understanding}, which says that an algorithm suffers from overfitting if the difference between the true risk and the empirical risk is large. The notion of ``large'' is vague and context-dependent but this definition is still useful for characterizing our problem.}. In other words, how accurately $\loss_{S}(\theta)$ approximates $\loss(\theta)$ depends on how well $S$ represents $\data$. This fact is formalized by the notion of \emph{representativeness} \citep{shalev2014understanding}. 

%TODO: check this definition
\textbf{Definition 1.} A training sample $S$ is $\epsilon$-representative with respect to a data distribution $\data$, iff, for every model parameter $\theta$, \footnote{We assume that the choices of model class, input domain, and loss function are fixed.}
$$|  \loss_{\data}(\theta) - \loss_{S}(\theta)| \leq \epsilon$$

If $\epsilon$ is small, representativeness suffices non-overfitting because an $\epsilon$-representative sample $S$ satisfies
$$|  \loss_{\data}(\hat{\theta}_S) - \loss_{S}(\hat{\theta}_S)| \leq \epsilon$$

%In other words, if a training sample $S$ is not $\epsilon$-representative for small $\epsilon$, it will cause the training algorithm to learn an overfitting model. 
We consider a relaxed definition of representativeness that takes into account the model parameter.

\textbf{Definition 2.} A training sample $S$ is $\epsilon$-representative with respect to a data distribution $\data$ and a model parameter $\theta$ iff
$$|  \loss_{\data}(\theta) - \loss_{S}(\theta)| \leq \epsilon$$ 

With this notion of representativeness, we can re-define \emph{overfitting} as follows.

\textbf{Definition 3.} A model $\hat{\theta}_{S}$, obtained by applying ERM on $S$,  overfits on $\data$ iff $S$ is \emph{not} $\epsilon$-representative with respect to $\data$ and $\hat{\theta}_S$ for small $\epsilon$.

When $\loss_{S}(\theta)$ is a consistent estimator of $\loss_{\data}(\theta)$, i.e. $\lim_{|S| \to \infty} \loss_{S}(\theta) =  \loss_{\data}(\theta)$, adding training examples to $S$ gradually closes the gap between them. We focus on a \emph{worse} case when $\loss_{S}(\theta)$ does not converge to $\loss_{\data}(\theta)$ as the size of $S$ increases because the sample is drawn from a distribution $\tdata \neq \data$, i.e. 
$$\lim_{|S| \to \infty} \loss_{S}(\theta) =  \loss_{\tdata}(\theta) \neq \loss_{\data}(\theta)$$

It is easy to see that, in this case, adding training examples cannot prevent overfitting. The following theorem implies that when $\loss_{\tdata}(\theta)$ deviates greatly from $\loss_{\data}(\theta)$, there is a very high chance of drawing an unrepresentative sample.  

\textbf{Theorem 1.} If $ |\loss_{\data}(\theta) - \loss_{\tdata}(\theta)| = \epsilon$, the probability of \emph{not} drawing an $\epsilon$-representative sample, with respect to $\data$ and $\theta$, from $\tdata$ is at least $\frac{1}{2}$.

\emph{Proof.} Let $S$ be a random sample drawn from $\tdata$ (the size of $S$ is fixed). 
Since $ \loss_{\data}(\theta) - \loss_{S}(\theta)$ is Gaussian distributed, $|\loss_{\data}(\theta) - \loss_{S}(\theta)|$ is folded-Gaussian distributed. 
The probability that $S$ is not $\epsilon$-representative w.r.t. $\data$ and $\theta$ is:
$$P \left(| \loss_{\data}(\theta) - \loss_{S}(\theta)| >  |\loss_{\data}(\theta) - \loss_{\tdata}(\theta)| \right)$$ 
It is easy to prove that this probability is at least $\frac{1}{2}$ by following the formula of the CDF of the folded Gaussian distribution and noticing that $\loss_{\data}(\theta) - \loss_{\tdata}(\theta)$ is the mean of the random variable $\loss_{\data}(\theta) - \loss_{S}(\theta)$.

\textbf{Collorary 1.} If $S$ is sampled from $\tdata$ and $ |\loss_{\data}(\hat{\theta}_S) - \loss_{\tdata}(\hat{\theta}_S)| = \epsilon$ for a large $\epsilon$, then there is at least $\frac{1}{2}$ chance that $\hat{\theta}_S$ overfits on $\data$. 

Therefore, optimizing for an incorrect objective severely deteriorates the quality of a model. 

\subsection{Methods}

\begin{figure*}[t]
  \centering
  \includegraphics[width=0.7\linewidth]{toyexample.pdf}
  \caption{A toy example to demonstrate that changing one feature can alter the class separator significantly. On the left is the case when a non-linearly separated dataset turns into a linearly separated one because the noisy representation of an input (bolded orange point) is observed instead of its true representation (bolded grey point). On the right is the opposite case when separating the two classes is much more difficult because of observing the noisy representation.  } 
  \label{fig:toy}
\end{figure*}


\subsubsection{Mismatched objectives}

In our problem setting, there is a mismatch between the objectives during training and testing because the feature vectors are drawn from distinct distributions in the two phases. Specifically, the training objective is
\begin{equation}
  \loss_{\data_{g}}(\theta) = \mathbb{E}_{(\xx, y) \sim \data_{g}} \left[ L(y, h_{\theta}(\xx)) \right]
\end{equation} whereas the testing objective is
\begin{equation}
  \loss_{\data_{p}}(\theta)  = \mathbb{E}_{(\xx, y) \sim \data_{p}} \left[ L(y, h_{\theta}(\xx)) \right]
\end{equation} where $\data_{g}$ is the gold feature distribution and $\data_{p}$ is the predicted feature distribution.

The gap between $\data_{g}$ and $\data_{p}$ depends on the accuracy of the feature classifiers, and is likely to cause the model perform poorly on unseen data if the predicted features are influential. Especially, when the classifiers are very inaccurate, even if the model encounters a raw input from the training sample, since the input's feature representation at testing time differs from what it is at training time, the model's prediction may also vary.  

\subsubsection{Can imitation learning techniques solve this problem?}

To bridge the gap between $\loss_{\data_g}(\theta)$ and $\loss_{\data_p}(\theta)$, a reasonable training objective is to directly minimize the loss function under the same distribution encountered at testing time:
\begin{equation}
  \begin{split}
  \hat{\theta} &= \arg\min_{\theta} \lpred(\theta) \\
  &= \mathbb{E}_{(\xx, y) \sim \data_p} \left[ L\left(y, h_{\theta}(\xx) \right) \right]  
  \end{split}
\end{equation}

This objective is widely adopted by imitation learning algorithms \citep{daume2009search, ross2011reduction}, which also target the problem of mismatched objectives. However, there is a major distinction between the imitation learning's problem setting and our problem setting. In imitation learning, because the model itself produces the predicted features, the gap between the gold and the predicted feature distributions is governed by its accuracy. As training progresses, the model becomes more accurate, thus the predicted distribution comes closer to being the gold distribution. Imitation learning techniques such as SEARN or DAgger unite the training and testing objectives by letting the predicted features be sampled from the same model in both phases. On the other hand, our problem is more general because the features can be predicted by arbitrary classifiers. If the feature classifiers are fixed and independent from the model (e.g. an off-the-shelf tool), imitation learning training techniques are ineffective because the testing (predicted) distribution is static regardless of how the model is trained.  The training-testing gap cannot be bridged unless the model is trained on only predicted features.

\subsubsection{Stochastic feature distribution}

To analyze effects of training on predicted features, we study a general \emph{stochastic feature distribution} $\data_{\alpha}$ that mixes the gold and the predicted feature distributions. This distribution is defined as follows: to extract features from a training example, toss an $\alpha$-biased coin to decide whether the predicted feature vector or the gold feature vector is extracted ($\alpha$ is the probability of using predicted features). 

Let $\theta_{\alpha}$ be the model that minimizes the loss under the distribution $\data_{\alpha}$:
\begin{equation}
  \begin{split}
  \theta_{\alpha} & = \arg\min_{\theta} \loss_{\data_{\alpha}}(\theta) \\
  &\eqdef \arg\min_{\theta} \mathbb{E}_{(\xx, y) \sim \data_{\alpha}} \left[ L\left(y, h_{\theta}(\xx) \right) \right]  
\end{split}
\end{equation}

From now on, for brevity, we will drop the argument $\theta_{\alpha}$ and use $\loss^{\alpha}$ and $h_{\alpha}$ to denote $\loss(\theta_{\alpha})$ and $h_{\theta_{\alpha}}$, respectively.  

The performance of $\theta_{\alpha}$ is measured by its loss under the testing (predicted) distribution, $\loss^{\alpha}_{\data_p}$. In order to explain the behavior of $\loss^{\alpha}_{\data_p}$ as $\alpha$ varies, we decompose this loss into two terms: a variance term and a bias term,
\begin{equation}
  \loss^{\alpha}_{\data_p} = \underbrace{\loss^{\alpha}_{\data_p} - \loss^{\alpha}_{\data_{\alpha}}}_{variance} + \underbrace{\loss^{\alpha}_{\data_{\alpha}}}_{bias}
  \label{eqn:bias_variance}
\end{equation}

Intuitively, the variance term measures the gap between the objective the model is optimizing for and the true objective. The bias term reflects the quality of the model, assuming that training and testing conditions ideally match. This term can be rewritten as \footnote{ We use $\data(\xx, y)$ to denote the probability of sampling $(\xx, y)$ from $\data$.}
\begin{align}
    \loss^{\alpha}_{\data_{\alpha}} &=  \mathbb{E}_{(\xx, y) \sim \data_{\alpha}} \left[ L(y, h_{\alpha}(\xx)) \right] \nonumber \\
    &= \sum_{(\xx, y)} L(y, h_{\alpha}(\xx)) \data_{\alpha}(\xx, y) \nonumber \\
    &= \sum_{(\xx_p, y_p)} L(y_p, h_{\alpha}(\xx_p)) \data_{\alpha}(\xx_p, y_p) \nonumber \\
    & + \sum_{(\xx_g, y_g)} L(y_g, h_{\alpha}(\xx_g)) \data_{\alpha}(\xx_g, y_g) \nonumber \\
    &= \alpha \sum_{(\xx_p, y_p)} L(y_p, h_{\alpha}(\xx_p)) \data_p(\xx_p, y_p) \nonumber \\
    & + (1 - \alpha) \sum_{(\xx_g, y_g)} L(y_g, h_{\alpha}(\xx_g)) \data_g(\xx_g, y_g) \nonumber \\
    &= \alpha \mathbb{E}_{(\xx_p, y_p) \sim \data_{p}} \left[ L(y_p, h_{\alpha}(\xx_g)) \right] \nonumber\\ 
    &  + (1 - \alpha)  \mathbb{E}_{(\xx_g, y_g) \sim \data_{g}} \left[ L(y_g, h_{\alpha}(\xx_g)) \right] \nonumber\\
    &= \alpha \loss^{\alpha}_{\data_p} + (1 - \alpha) \loss^{\alpha}_{\data_g}
\label{eqn:own_loss}
\end{align} which is a weighted sum of the loss on the predicted distribution and the loss on the gold distribution. 

As $\alpha$ approaches 1, the variance term converges to 0 but the bias term may not. The reason is that the behavior of $\loss_{\data_g}$ depends on whether the gold or predicted features provides a better representation of the data (Figure \ref{fig:toy}). Hence, it is not necessarily optimal setting $\alpha$ equal to 1 and training only on predicted features.  

\subsubsection{A grid search approach}
\begin{algorithm}[t]
  \small
  \begin{algorithmic}[1]
    \Function{GridSearch}{$\delta$, $T$, $D_p$, $D_g$}
    \State Set $\alpha = 0$
    \State Initialize $\theta^{*}$
    \While {$\alpha \leq 1$}
    \State $\theta_{\alpha}$ = \Call{Train}{$\alpha$, $T$, $D_p$, $D_g$}
    \If {$\theta_{\alpha}$ outperforms $\theta^{*}$ on valid set}
    \State Update $\theta^{*} = \theta_{\alpha}$
    \EndIf
    \State $\alpha = \alpha + \delta$
    \EndWhile
  \State Report performance of $\theta^{*}$ on test set.
  \State \Return $\theta^{*}$
  \EndFunction
\end{algorithmic}
\caption{Grid search algorithm.}
\label{alg:gridsearch}
\end{algorithm} 


\begin{algorithm}[t]
  \small
  \begin{algorithmic}[1]
    \Function{Train}{$\alpha$, $T$, $D_p$, $D_g$}
    \State Initialize $\theta_{\alpha}$
    \For {$ t = 0\dots T - 1 $}
    \State $D_{\alpha} = \emptyset$ 
    \For {$ (\xx_i, y_i) \in D_p$, $(\xx'_i, y_i) \in D_g$}
    \State Uniformly randomize $\beta \in [0, 1)$
      \If {$\beta < \alpha$}
      \State $D_{\alpha} = \data_{\alpha} \cup (\xx_i, y_i)$  
      \Else
      \State $D_{\alpha} = \data_{\alpha} \cup (\xx'_i, y_i)$
    \EndIf
  \EndFor
  \State Update $\theta_{\alpha}$ using $D_{\alpha}$
\EndFor
\State \Return $\theta_{\alpha}$
  \EndFunction
\end{algorithmic}
\caption{Training procedure.}
\label{alg:train}
\end{algorithm}


We propose a grid search method to find the value of $\alpha$ that minimizes $\loss^{\alpha}_{\data_p}$ (Algorithm \ref{alg:gridsearch} and \ref{alg:train}). As named, our method conducts a grid search on $\alpha$ over the interval $[0, 1]$. It requires two training sets $D_p$, containing predicted feature vectors, and $D_g$, containing gold feature vectors, to be generated beforehand. During training, given a fixed value of $\alpha$, the algorithm loops for $T$ iterations. At each iteration, it samples a dataset $D_{\alpha}$ from the stochastic distribution $\data_{\alpha}$ by repeatedly tossing an $\alpha$-biased coin to decide which training set to draw an example from. It then updates the model parameter using $D_{\alpha}$. In the outer loop of $\alpha$, the model that gives the best validation performance is selected for testing. Note that all performances are measured under testing conditions, i.e. with predicted features.



\section{Experiments}

\subsection{Data and tools}

%Very briefly, what data did you use?
%If you had to collect your own, how did you do this?

We use the CoNLL 2007 dependency parsing dataset \citep{nilsson2007conll} in our experiments \footnote{We obtain this dataset from CMSC 723.}. The dataset consists of 13970 sentences, annotated with their dependency parses and POS tags. We split the data into 10000 sentences for training, 982 sentences for validation, and 2988 sentences for testing.

We re-implement a perceptron shift-reduce dependency parsing model with the base line feature template described in \citep{zhang2011transition}. To measure the performance of this model, we use the standard unlabeled attachment score (UAS), which is the percentage of tokens having correct heads. 

We choose POS tags to be the features to be predicted because they are very crucial features for solving syntactic tasks such as dependency parsing. To obtain POS tag predictions, we use the NLTK's tagger \citep{bird2009natural}, which implements a greedy averaged perceptron algorithm. We also employ the CRFsuite package \citep{okazaki2007crfsuite} to train a separate POS tagger on the CoNLL 2000 chunking data set \citep{tjong2000introduction}, which is released with NLTK. This dataset is divided into 7000 sentences for training, and 3948 sentences for testing. 

Since our grid search algorithm involves randomness from sampling the feature vectors \footnote{Our averaged perceptron implementation also randomly shuffles training examples at the beginning of each iteration but we find that setting different random seeds does not affect the final predictions (possibly due to averaging).},  we perform 10 runs of our experiments and report the mean results with 95\% confidence intervals.  

\begin{figure}[t]
  \centering
  \includegraphics[width=0.75\linewidth]{exp1_1.pdf}
  \caption{Performance of the dependency parser on noisy POS features. The POS error rates are measured on the CoNLL 2000 test set.}
  \label{fig:exp1}
\end{figure}


\begin{figure}[t]
  \centering
  \includegraphics[width=0.75\linewidth]{exp2_1.pdf}
  \caption{Performance of the dependency parser with grid search.}
  \label{fig:exp2}
\end{figure}


\begin{figure}[t]
  \centering
  \includegraphics[width=0.75\linewidth]{exp3.pdf}
  \caption{Performance gain of training with grid search against training only on gold features under various noise levels. }
  \label{fig:exp3}
\end{figure}

\begin{figure}[t]
  \centering
  \includegraphics[width=0.75\linewidth]{exp4.pdf}
  \caption{Performance of the parsing model trained by DAgger after 10 iterations. }
  \label{fig:exp4}
\end{figure}


\subsection{Evaluating methodology}

The success of this work is measured by how thorough these objectives are fulfilled: (1) verifying theoretical claims in practice, (2) analyzing the effectiveness of the proposed solution (3) implementing tools to conduct experiments and (4) gaining practical insights from experimental results.

\subsection{Effect of noisy features on model performance}
\label{sec:noise_is_bad}

We measure the performance of the dependency parser under diverse levels of accuracy of the POS tagger. To control for the accuracy of the tagger, we train a CRF trigram model with various training sizes, ranging from $1000$ to $7000$. We then use each tagging model to generate (noisy) POS tag features on the parsing validation and test sets. As the training size decreases, we expect to obtain less accurate features and to observe performance of a parser trained only on gold features decrease on validation and test data. 

Figure \ref{fig:exp1} shows the result of this experiment. As expected, the model performance degrades monotonically as the tagger makes more mistakes. We also notice that the correlation between the two axes is approximately linear.

\begin{figure*}[t]
  \centering
  \begin{subfigure}{0.44\linewidth}
  \includegraphics[width=0.8\linewidth]{exp2_2.pdf}
  \subcaption{}
\end{subfigure}
\begin{subfigure}{0.44\linewidth}
  \includegraphics[width=0.8\linewidth]{exp2_3.pdf}
  \subcaption{}
\end{subfigure}
\caption{Behaviors of bias and variance of the parsing model as $\alpha$ increases.}
\label{fig:bias_variance}
\end{figure*}

\subsection{Effectiveness of grid search}


We train parsing models using the grid search algorithm with $\alpha$ varying from 0 to 1. We report the UAS scores of the models on the validation and test sets under testing conditions. As seen from Table \ref{tab:run}, the grid search algorithm consistently outperforms the other two naive approaches, which train models on solely on gold or predicted features. Generally, the model performance tends to improve as $\alpha$ increases but the improvement gaps are not uniform. The performance is boosted substantially when $\alpha$ goes from 0 to 0.5 but then stagnates at higher values and slightly goes down at the end (although not statistically significant). 
In 9 out of 10 runs, the optimal $\alpha$ is between 0 and 1 exclusively. Moreover, the models lean toward using more predicted features than gold features ($\alpha$ is usually around 0.7-0.8). We also find that always using predicted features for training the model is superior to always using gold features. In fact, the gap between using the optimal $\alpha$ and simply setting $\alpha = 1$ is not statistically significant (Figure \ref{fig:exp2}). Hence, we recommend that in cases when running the grid search algorithm is too expensive, simply training with predicted features would be a decent choice. 

We examine the effectiveness of grid search under various levels of noise. We conduct an experiment similar to the one Section \ref{sec:noise_is_bad} but, in addition, we train a model with grid search on each level of noise. We observe that grid search becomes more effective as the POS features become noisier (Figure \ref{fig:exp3}).  

To compare grid search with an imitation learning approach, we apply a schedule to $\alpha$ in Algorithm \ref{alg:train}, changing its value after each iteration. This modified algorithm mimics the online version of DAgger where, at each iteration, the model parameter is updated without having to aggregate all data from the past and retraining from scratch. We use the schedule recommended by \cite{ross2011reduction}, setting $\alpha = 0.9^t$ during the $t$-th iteration. The performance of this algorithm after 10 training iterations is reported in Figure \ref{fig:exp4}. As seen, the model does not improve after iteration 2. We speculate that this is because, at iteration 2, $\alpha = 0.73$, which is close to optimal $\alpha$ detected by grid search. As the $\alpha$ deviates from this optimal value, the model performance drops. Hence, the model performance is strongly correlated with the value of $\alpha$ rather than the number of iterations.  

\begin{table}[t]
  \centering
  \renewcommand{\arraystretch}{1.2}
    \begin{tabular}{ccccc}
    \Xhline{2\arrayrulewidth}
    Run & Gold  & Predicted & Grid search & $\alpha_{opt}$ \\ \Xhline{2\arrayrulewidth}
    1   & 85.96     & 87.27     & \textbf{87.37}  & 0.7   \\ \hline
    2   & 85.66     & \textbf{87.30}     & 87.07  & 0.3  \\ \hline
    3   & 85.68     & 87.22     & \textbf{87.38}  & 0.5   \\ \hline
    4   & 85.87     & \textbf{87.28}     & \textbf{87.28} & 1.0  \\ \hline
    5   & 85.87     & 87.27     & \textbf{87.31}  & 0.8   \\ \hline
    6   & 85.73     & 87.11     & \textbf{87.17}  & 0.8   \\ \hline
    7   & 85.81     & 87.03     & \textbf{87.18}  & 0.7  \\ \hline
    8   & 85.80     & \textbf{87.42}     & 87.33  & 0.7  \\ \hline
    9   & 85.73     & 87.22     & \textbf{87.33}  & 0.8   \\ \hline
    10  & 85.82     & \textbf{87.32}     & \textbf{87.32} & 0.7  \\ \Xhline{2\arrayrulewidth}
    \end{tabular}
    \caption{Parsing results on the test set over 10 runs of the grid search algorithm. Best results in each run are highlighted.}
    \label{tab:run}
\end{table}


\subsection{Behaviors of bias and variance}

We calculate the variance term according to Equation \ref{eqn:bias_variance}, and interpolate the bias term using Equation \ref{eqn:own_loss} \footnote{The formula still applies when we replace the loss function in the equation by the UAS metric.}. The goal is to verify whether the variance term converges to 0 as $\alpha$ goes to 1 and to study changes in the bias term. 

For this task, there is clearly a tradeoff between bias and variance. The variance term converges to 0 as the algorithm uses more predicted features (Figure \ref{fig:bias_variance}a), demonstrating that doing so is effective in bridging the training-test gap. On the other hand, the bias term exhibits a clear trend of escalating (Figure \ref{fig:bias_variance}b), hinting that the noisy features potentially create outliers and hinder learning. Although not surprising, this observation confirms that gold features produce better representations than predicted features do on this dataset.  

\subsection{Error Analysis}

We also perform preliminaries experiments on sentiment analysis with noisy POS tags. However, we do not see a significant gain in accuracy when varying $\alpha$. We think there are two possible explanations. First, syntactic features such as POS tags are not as useful in sentiment analysis, which is an semantic-oriented task, as they are in dependency parsing, which is a syntactic-oriented task. We confirm this fact by removing the POS features from the model and not seeing a substantial loss in accuracy. Second, the model itself is not very accurate, achieving an F-1 score below 80\%, as we do not have time to construct a powerful feature template for it. We decide not to put sentiment analysis in the experiment section because further investigations are required to validate these results.      

\subsection{Doing it Again}

The experiment section focuses entirely on a single task so the conclusions may not generalize. We would like to apply the grid search algorithm to other tasks. It could be a task where the algorithm gives neutral results such as sentiment analysis (discussed above) or another task that possibly gives positive results such as coreference resolution (with noisy named entity features).   

One concern we have in this experiment section is that we do not account for the errors in the approximations of the bias term. The concern originates from the fact that we approximate the bias term not actually from $\loss^{\alpha}_{\data_p}$ and $\loss^{\alpha}_{\data_g}$, but from empirical losses on finite datasets.

We also wonder if training with grid search would enhance any other properties of the model beside performance, such as accuracy in probability estimation. We are highly motivated by the fact that, although most NLP research pays attention to producing accurate instance-specific predictions, for other related fields such as computational social science, a higher objective is to use those predictions to reason about a particular population. For example, we may be interested in computing the marginal probability of a quantity of interest over some population. Having accurate probability estimations is extremely useful in this case. This is an interesting problem and will be left as future works.     

\section{Relation to Class}

With this work, we employ knowledge about learning theory (risk minimization, overfitting, representativeness, bias-variance tradeoff), imitation learning (mismatched objectives, DAgger), multiclass learning (averaged perceptron algorithm). So we hope that this work will make Hal happy since he did a great job at teaching us that many concepts within a semester. We learn a lot from this class!  

\bibliographystyle{plainnat}
\bibliography{project}

\end{document}
