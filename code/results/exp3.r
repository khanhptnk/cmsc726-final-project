library(ggplot2)

data <- read.table("exp3.log")
print(data)

data$c <- rep(2, 7)

ggplot(data, aes(V1)) +
      geom_line(aes(y=V2, colour="green"), size=2) + 
      geom_point(aes(y=V2), size=3) + 
      geom_errorbar(aes(ymin=V2-V3, ymax=V2+V3), width=0.2, size=0.8) + 
      xlab("POS error rate (%)") + 
      ylab("Performance gain (%)") + 
      scale_color_brewer(palette="Set2") +
      theme(text=element_text(size=20),
            legend.title=element_blank(), 
            legend.position="none",
            legend.text=element_text(size=20), 
            legend.key.height=unit(2, "line"),
            legend.key.width=unit(2, "line"))

ggsave("../../writing/exp3.pdf")
