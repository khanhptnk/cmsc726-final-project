library(ggplot2)

data <- read.table("exp4.log")

ggplot(data, aes(V1)) +
      geom_line(aes(y=V2, colour="Valid"), size=2) + 
      geom_line(aes(y=V4, colour="Test"), size=2) + 
      geom_point(aes(y=V2), size=3) +
      geom_point(aes(y=V4), size=3) + 
      geom_errorbar(aes(ymin=V2-V3, ymax=V2+V5), width=0.2, size=0.8) +
      geom_errorbar(aes(ymin=V4-V5, ymax=V4+V5), width=0.2, size=0.8) +
      xlab("Iteration") + 
      ylab("UAS score (%)") + 
      scale_x_continuous(breaks=c(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)) + 
      theme(text=element_text(size=20),
            legend.title=element_blank(), 
            legend.position=c(0.8, 0.1),
            legend.text=element_text(size=20), 
            legend.key.height=unit(2, "line"),
            legend.key.width=unit(2, "line"))

ggsave("../../writing/exp4.pdf")



