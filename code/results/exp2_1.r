library(ggplot2)

devmean <- read.table("exp2.dev.mean")
devse <- read.table("exp2.dev.se")
testmean <- read.table("exp2.test.mean")
testse <- read.table("exp2.test.se")

data <- data.frame(matrix(nrow=11, ncol=5))

data$X1 <- devmean$V1
data$X2 <- devmean$V2
data$X3 <- devse$V2

data$X4 <- testmean$V2
data$X5 <- testse$V2

print(data$X2 + data$X3)

ggplot(data, aes(X1)) +
      geom_line(aes(y=X2, colour="Valid"), size=2) + 
      geom_line(aes(y=X4, colour="Test"), size=2) + 
      geom_point(aes(y=X2), size=3) +
      geom_point(aes(y=X4), size=3) + 
      geom_errorbar(aes(ymin=X2-X3, ymax=X2+X3), width=0.02, size=0.8) +
      geom_errorbar(aes(ymin=X4-X5, ymax=X4+X5), width=0.02, size=0.8) +
      xlab("Alpha") + 
      ylab("UAS score (%)") + 
      theme(text=element_text(size=20),
            legend.title=element_blank(), 
            legend.position=c(0.9, 0.1),
            legend.text=element_text(size=20), 
            legend.key.height=unit(2, "line"),
            legend.key.width=unit(2, "line"))

ggsave("../../writing/exp2_1.pdf")



