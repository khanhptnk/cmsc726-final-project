import numpy as np
from scipy.stats import sem

dev = [None] * 5
test = [None] * 5
for run in xrange(5):
    dev[run] = [None] * 11
    for alpha in xrange(11):
        dev[run][alpha] = [None] * 10
    test[run] = [None] * 11
    for alpha in xrange(11):
        test[run][alpha] = [None] * 10

def readMetrics(f, run, data):
    for alpha in xrange(11):
        nums = [float(x) for x in f.readline().split()]
        assert len(nums) == 5
        for metric in xrange(5):
            data[metric][alpha][run] = nums[metric]

def CI(x):
    return 2 * sem(x)

def compute(filename, data, func):
    with open(filename, "w") as w:
        for alpha in xrange(11):
            for metric in xrange(5):
                if (metric == 4):
                    data[metric][alpha] = [abs(x) for x in data[metric][alpha]]
                print >> w, func(data[metric][alpha]),
            print >> w, ""

alphas = []
with open("exp2.log", "r") as f:
    for run in xrange(10):
        for j in xrange(7):
            f.readline()
        readMetrics(f, run, dev)
        alphas.append(float(f.readline().split(" = ")[1]))
        for j in xrange(2):
            f.readline()
        readMetrics(f, run, test)
#for run in xrange(10):
#    print "%.2f %.2f %.2f %.1f" % (test[1][0][run], test[1][10][run], test[1][int(alphas[run] * 10)][run], alphas[run])

compute("exp2.dev.mean", dev, np.mean)
compute("exp2.dev.se", dev, CI)
compute("exp2.test.mean", test, np.mean)
compute("exp2.test.se", test, CI)

        

