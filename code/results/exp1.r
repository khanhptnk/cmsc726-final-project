library(ggplot2)

data <- read.table("exp1.log")
print(data)

ggplot(data, aes(V1)) +
      geom_line(aes(y=V2, colour = "Valid"), size=2) + 
      geom_point(aes(y=V2), size=3) + 
      geom_line(aes(y=V3, colour = "Test"), size=2) +
      geom_point(aes(y=V3), size=3) + 
      xlab("POS error rate (%)") + 
      ylab("UAS score (%)") + 
      theme(text=element_text(size=20),
            legend.title=element_blank(), 
            legend.position=c(0.1, 0.1),
            legend.text=element_text(size=20), 
            legend.key.height=unit(2, "line"),
            legend.key.width=unit(2, "line"))

ggsave("../../writing/exp1_1.pdf")
