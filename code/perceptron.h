#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <random>
#define SparseIntVec unordered_map<int, int>
#define SparseFloatVec unordered_map<int, float>
#define Example pair<SparseIntVec, int>
using namespace std;

class Perceptron {
 public:
  int MAX_ITER = 10;
  int numClasses;
  Perceptron(int _numClasses) {
    numClasses = _numClasses;
    weights = new SparseFloatVec[numClasses];
    sumWeights = new SparseFloatVec[numClasses];
  }
  /*~Perceptron() {
    delete[] weights;
    delete[] sumWeights;
  }*/
  void update(int cnt, Example* example);
  void iterate(vector<Example> &trainGold, vector<Example> &trainPred, vector<int> &indices, float alpha);
  void trainNoisy(vector<Example> &trainGold, vector<Example> &trainPred, float alpha, int seed);
  void average();
  void unaverage();
  int predict(SparseIntVec &x);
  float computeLoss(vector<Example> &data);
 private:
  int cnt = 1;
  SparseFloatVec* weights;
  SparseFloatVec* sumWeights;
  float dot(SparseFloatVec &weight, SparseIntVec &x);
  void addWeight(SparseFloatVec &weight, SparseIntVec &x, int v);
};
