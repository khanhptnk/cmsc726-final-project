import sys
inp = sys.argv[1]

data = []
sent = []
with open(inp, "r") as f:
    for line in f:
        line = line.rstrip("\n")
        if (line == ""):
            data.append(sent)
            sent = []
            continue
        fields = line.split("\t")
        del fields[9]
        del fields[8]
        del fields[7]
        del fields[5]
        del fields[3]
        del fields[2]
        tmp = fields[2]
        fields[2] = fields[3]
        fields[3] = tmp
        sent.append(fields)

import nltk

totalCorrect = 0
totalPred = 0

out = sys.argv[2]
with open(out, "w") as w:
    for sent in data:
        text = []
        for fields in sent:
            text.append(fields[1])
        predTags = nltk.pos_tag(text)
        totalPred += len(predTags)
        for i in xrange(len(sent)):
            totalCorrect += (predTags[i][1] == sent[i][3])
            sent[i].append(predTags[i][1])
            print >> w, "\t".join(sent[i])
        print >> w, ""

print "Accuracy = %0.2f" % (totalCorrect * 1.0 / totalPred)
