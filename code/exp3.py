import numpy as np
from scipy.stats import sem

def readMetrics(f, run, data):
    for alpha in xrange(11):
        nums = [float(x) for x in f.readline().split()]
        assert len(nums) == 5
        for metric in xrange(5):
            data[metric][alpha][run] = nums[metric]

def CI(x):
    return 2 * sem(x)

def compute(filename, data, func):
    with open(filename, "w") as w:
        for alpha in xrange(11):
            for metric in xrange(5):
                if (metric == 4):
                    data[metric][alpha] = [abs(x) for x in data[metric][alpha]]
                print >> w, func(data[metric][alpha]),
            print >> w, ""

data = [None] * 7
for size in xrange(7):
    data[size] = [None] * 10

for run in xrange(10):
    filename = "exp3_" + str(run) + ".log"
    with open(filename, "r") as f:
        f.readline()
        for size in xrange(7):
            for i in xrange(15):
                f.readline()
            fields = f.readline().rstrip().split()
            data[size][run] = abs(float(fields[0]) - float(fields[1]))

for size in reversed(xrange(7)):
    print np.mean(data[size]), sem(data[size])

        

