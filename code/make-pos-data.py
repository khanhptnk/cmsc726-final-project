from nltk.corpus import conll2000

def printData(filename, data):
    with open(filename, "w") as w:
        for sent in data:
            for pair in sent:
                print >> w, pair[1] + "\t" + pair[0]
            print >> w, ""


sents = conll2000.tagged_sents()
total = len(sents)
print total

train = sents[:7000]
test = sents[7000:]

printData("pos.tr", train)
printData("pos.te", test)


