#!/bin/bash

python extract-crf-feature.py pos.te
TEST=pos.te.crf

DEPDATA=../data
DEPTR=$DEPDATA/data.tr
DEPDE=$DEPDATA/data.dev
DEPTE=$DEPDATA/data.te

cut -d$'\t' -f2 $DEPTR > datatr
python extract-crf-feature.py datatr

cut -d$'\t' -f2 $DEPDE > datadev
python extract-crf-feature.py datadev

cut -d$'\t' -f2 $DEPTE > datate
python extract-crf-feature.py datate


for i in `seq 1000 1000 7000`; do 

        echo $i

        python cut-sent.py pos.tr $i > traintmp
        python extract-crf-feature.py traintmp
        rm -rf traintmp

        TRAIN=traintmp.crf
        MODEL=pos_${i}.model
        
        if [ ! -f $MODEL ] ; then
                $CRF_SUITE/crfsuite learn -m $MODEL $TRAIN > /dev/null
                rm -rf traintmp.crf
        fi

        $CRF_SUITE/crfsuite tag -qt -m $MODEL $TEST | tail -3 | head -1 

        $CRF_SUITE/crfsuite tag -m $MODEL datatr.crf > tmp
        cut -d$'\t' -f1-4 $DEPTR | paste -d$'\t' - tmp > ${DEPTR}.${i}
        sed -i 's/^\t$//g' ${DEPTR}.${i} 
        rm -rf tmp

        $CRF_SUITE/crfsuite tag -m $MODEL datadev.crf > tmp
        cut -d$'\t' -f1-4 $DEPDE | paste -d$'\t' - tmp > ${DEPDE}.${i}
        sed -i 's/^\t$//g' ${DEPDE}.${i}
        rm -rf tmp

        $CRF_SUITE/crfsuite tag -m $MODEL datate.crf > tmp
        cut -d$'\t' -f1-4 $DEPTE | paste -d$'\t' - tmp > ${DEPTE}.${i}
        sed -i 's/^\t$//g' ${DEPTE}.${i}
        rm -rf tmp
 done
 
 rm -rf datatr.crf datadev.crf datate.crf
