#include <vector>
#include "perceptron.h"

float Perceptron::dot(SparseFloatVec &weight, SparseIntVec &x) {
  float ans = 0.;
  if (x.size() < weight.size()) {
    for (auto& pair : x) {
      if (weight.find(pair.first) != weight.end()) {
        ans += weight[pair.first];
      }
    }
  } else {
    for (auto& pair : weight) {
      if (x.find(pair.first) != x.end()) {
        ans += pair.second;
      }
    }
  }
  return ans;
}

void Perceptron::addWeight(SparseFloatVec &weight, SparseIntVec &x, int v) {
 for (auto& pair : x) {
   if (weight.find(pair.first) == weight.end()) {
     weight[pair.first] = 0;
   }
   weight[pair.first] += v;
 }
}

int Perceptron::predict(SparseIntVec &x) {
  int ans = 0;
  float bestVal = dot(weights[0], x);
  for (int y = 1; y < numClasses; y++) {
    float curVal = dot(weights[y], x);
    if (curVal > bestVal) {
      bestVal = curVal;
      ans = y;
    }
  }
  return ans;
}

void Perceptron::update(int cnt, Example *example) {
  SparseIntVec x = example->first;
  
  int yPred = predict(x);
  int y = example->second;
  if (yPred != y) {
    addWeight(weights[yPred], x, -1);
    addWeight(weights[y], x, 1);
    addWeight(sumWeights[yPred], x, -cnt);
    addWeight(sumWeights[y], x, cnt);
  }
 }

void Perceptron::iterate(vector<Example> &trainGold, vector<Example> &trainPred, vector<int> &indices, float alpha) {
    for (auto& idx : indices) {
      double randNum = (double) rand() / RAND_MAX;
      Example example = trainGold[idx];
      if (randNum < alpha) example = trainPred[idx];
      update(cnt++, &example);
    }

}

void Perceptron::average() {
  for (int i = 0; i < numClasses; i++) {
    for (auto& pair : sumWeights[i]) {
      int tmp = weights[i][pair.first];
      weights[i][pair.first] -= sumWeights[i][pair.first] / cnt;
      sumWeights[i][pair.first] = tmp;
    }
  }
}

void Perceptron::unaverage() {
  for (int i = 0; i < numClasses; i++) {
    for (auto& pair : sumWeights[i]) {
      int tmp = weights[i][pair.first];
      weights[i][pair.first] = sumWeights[i][pair.first];
      sumWeights[i][pair.first] = (sumWeights[i][pair.first] - tmp) * cnt;
    }
  }
}

void Perceptron::trainNoisy(vector<Example> &trainGold, vector<Example> &trainPred, float alpha, int seed) {
  srand(seed);
  vector<int> indices;
  for (int i = 0; i < (int)trainGold.size(); i++) {
    indices.push_back(i);
  }
  for (int iter = 0; iter < MAX_ITER; iter++) {
    random_shuffle(indices.begin(), indices.end());
    iterate(trainGold, trainPred, indices, alpha);
  }
  average();
}

float Perceptron::computeLoss(vector<Example> &data) {
  float loss = 0.;
  for (auto& example : data) {
    float maxScore = -1e9;
    float trueScore = -1;
    for (int y = 0; y < numClasses; y++) {
      float score = dot(weights[y], example.first);
      if (example.second == y) trueScore = score;
      maxScore = max(maxScore, score);
    }
    loss += maxScore - trueScore;
  }
  //loss /= data.size();
  return loss;
}
