import numpy as np
from scipy.stats import sem

dev = [None] * 10
for iteration in xrange(10):
    dev[iteration] = [None] * 10

test = [None] * 10
for iteration in xrange(10):
    test[iteration] = [None] * 10

for run in xrange(10):
    filename = "exp4_" + str(run) + ".log"
    with open(filename, "r") as f:
        for i in xrange(6):
            f.readline()
        for iteration in xrange(10):
            fields = f.readline().rstrip().split()
            dev[iteration][run] = float(fields[1])
            test[iteration][run] = float(fields[2])

for iteration in xrange(10):
    print iteration, np.mean(dev[iteration]), sem(dev[iteration]), np.mean(test[iteration]), sem(dev[iteration])

        

