import sys
from nltk.tokenize import blankline_tokenize

def extractFeatures(position, pairs):
    ans = []
    ans.append("w[0]=" + str(pairs[position][-1].lower()))
    if (position > 0):
        ans.append("w[-1]=" + pairs[position - 1][-1].lower())
    if (position > 1):
        ans.append("w[-2]=" + pairs[position - 2][-1].lower())
    if (position + 1 < len(pairs)):
        ans.append("w[+1]=" + pairs[position + 1][-1].lower())
    if (position + 2 < len(pairs)):
        ans.append("w[+2]=" + pairs[position + 2][-1].lower())
    return "\t".join(ans)

def process(inp, out):
    with open(out, "w") as w:
        tweets = blankline_tokenize(open(inp, "r").read())
        for tweet in tweets:
            tweet = tweet.replace(":", "\\:").replace("\\", "\\\\")
            pairs = []
            for position in tweet.split("\n"):
                pairs.append(position.split("\t"))
            for i, pair in enumerate(pairs):
                print >> w, pair[0] + "\t" + extractFeatures(i, pairs)
            print >> w, ""

inp = sys.argv[1]

process(inp, inp + ".crf")

