from nltk.tokenize import blankline_tokenize

def printSents(filename, data):
    with open(filename, "w") as w:
        for sent in data:
            print >> w, sent
            print >> w, ""

sents = blankline_tokenize(open("data", "r").read()) 
print len(sents)

train = sents[:10000]
test = sents[10000:]

printSents("data.tr", train)
printSents("data.te", test)
