#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <unordered_map>
#include <vector>
#include <string>
#include <cstring>
#include <cstdlib>
#include <iomanip>      // std::setw
#include "perceptron.h"
#define NUMCLASSES 3
#define MAXL 200
#define SparseIntVec unordered_map<int, int>
#define Example pair<SparseIntVec, int>  
#define Edge pair<int, int>
using namespace std;

int stckHead, buffHead;
int stck[MAXL], buff[MAXL];
int outDeg[MAXL];
unordered_map<string, int> featureMap;

class Graph {
 public:
  vector< unordered_map<string, string> > nodes;
  set <pair<int, int> > edges;
  void addNode(unordered_map<string, string> node) {
    nodes.push_back(node);
  }
  void addEdge(int head, int tail) {
    edges.insert(make_pair(head, tail));
  }
  bool hasEdge(int head, int tail) {
    return edges.find(make_pair(head, tail)) != edges.end();
  }
  void removeAllEdges() {
    edges.clear();
  }
  void finalize() {
    vector<int> leftmost(nodes.size(), 1000000000);
    vector<int> rightmost(nodes.size(), -1);
    for (auto& edge : edges) {
      leftmost[edge.first] = min(leftmost[edge.first], edge.second);
      rightmost[edge.first] = max(rightmost[edge.first], edge.second);
    }
    for (int i = 0; i < (int)nodes.size(); i++) {
      nodes[i]["left"] = leftmost[i] != 1000000000 ? to_string(leftmost[i]) : "-1";
      nodes[i]["right"] = to_string(rightmost[i]);
    }
  }
  void print() {
    for (auto& node : nodes) {
      for (auto& entry : node) {
        cout << entry.first << ' ' << entry.second << endl;
      }
    }
    for (auto& edge : edges) {
      cout << edge.first << ' ' << edge.second << endl;
    }
    cout << endl;
  }
};

vector<Graph*> trainGoldGraphs, trainPredGraphs;
vector<Graph*> devGoldGraphs, devPredGraphs;
vector<Graph*> testGoldGraphs, testPredGraphs;
vector< Example > trainGold, trainPred; 

void readData(const string &filename, vector<Graph*> *goldGraphs, vector<Graph*> *predGraphs) {
  ifstream file(filename);
  string idx, word, parent, goldpos, predpos;
  string line;
  
  Graph* goldGraph = new Graph;
  Graph* predGraph = new Graph;
 
  unordered_map<string, string> node;
  node["idx"] = "0";
  node["word"] = "*ROOT*";
  node["pos"] = "*ROOT*";
  node["head"] = "-1";
  goldGraph->addNode(node);
  predGraph->addNode(node);


  while (getline(file, line)) {
      if (line.length() == 0) {
        
        goldGraph->finalize();
        predGraph->finalize();
        goldGraphs->push_back(goldGraph);
        predGraphs->push_back(predGraph);
        
        goldGraph = new Graph;
        predGraph = new Graph;
        
        node["idx"] = "0";
        node["word"] = "*ROOT*";
        node["pos"] = "*ROOT*";
        node["head"] = "-1";     
        goldGraph->addNode(node);
        predGraph->addNode(node);
        continue;
      }
      istringstream lineStream(line);
      lineStream >> idx >> word >> parent >> goldpos >> predpos;
      
      node["idx"] = idx;
      node["word"] = word;
      node["pos"] = goldpos;
      node["head"] = parent;
      
      goldGraph->addNode(node);
      node["pos"] = predpos; 
      predGraph->addNode(node);
     
      goldGraph->addEdge(stoi(parent), stoi(idx));  
      predGraph->addEdge(stoi(parent), stoi(idx));
  }
  file.close();
}


void extractFeatures(Graph *graph, SparseIntVec &x) {
  vector<string> features;

  // single words
  int stckNode = stckHead ? stck[stckHead - 1] : -1;
  int head, left, right;
  head = left = right = -1;
  string sw, sp, shp, slp, srp;
  shp = slp = srp = "NONE";
  if (stckNode != -1) {
    sw =  graph->nodes[stckNode]["word"];
    sp = graph->nodes[stckNode]["pos"];
    
    head = stoi(graph->nodes[stckNode]["head"]);
    if (head != -1) shp = graph->nodes[head]["pos"];
    
    left = stoi(graph->nodes[stckNode]["left"]);
    if (left != -1) slp = graph->nodes[left]["pos"];
    
    right = stoi(graph->nodes[stckNode]["right"]);
    if (right != -1) srp = graph->nodes[right]["pos"];

    features.push_back("sw_" + sw);
    features.push_back("sp_" + sp);
    features.push_back("swp_" + sw + "_" + sp);
  } else {
    sw = sp = "NONE";
  }

  int buffNode = buffHead ? buff[buffHead - 1] : -1;
  string bw, bp;
  if (buffNode != -1) {
    bw = graph->nodes[buffNode]["word"];
    bp = graph->nodes[buffNode]["pos"];
    
    features.push_back("bw_" + bw);
    features.push_back("bp_" + bp);
    features.push_back("bw_bp_" + bw + "_" + bp);
  } else {
    bw = bp = "NONE";
  }

  int buffNode_1 = buffHead > 1 ? buff[buffHead - 2] : -1;
  string bw1, bp1;
  if (buffNode_1 != -1) {
    bw1 = graph->nodes[buffNode_1]["word"];
    bp1 = graph->nodes[buffNode_1]["pos"];

    features.push_back("bword_" + bw1);
    features.push_back("bpos_" + bp1);
    features.push_back("bword_bpos_" + bw1 + "_" + bp1);
  } else {
    bw1 = bp1 = "NONE";
  }

  int buffNode_2 = buffHead > 2 ? buff[buffHead - 3] : -1;
  string bw2, bp2;
  if (buffNode_2 != -1) {
    bw2 = graph->nodes[buffNode_2]["word"];
    bp2 = graph->nodes[buffNode_2]["pos"];

    features.push_back("bword_" + bw2);
    features.push_back("bpos_" + bp2);
    features.push_back("bword_bpos_" + bw2 + "_" + bp2);
  } else {
    bw2 = bp2 = "NONE";
  }

  // pairs
  if (stckNode != -1 && buffNode != -1) {
    features.push_back("swpbwp_" + sw + "_" + sp + "_" + bw + "_" + bp);
    features.push_back("swpbw_" + sw + "_" + sp + "_" + bw);
    features.push_back("swbwp_" + sw + "_" + bw + "_" + bp);
    features.push_back("swpbp_" + sw + "_" + sp + "_" + bp);
    features.push_back("spbwp_" + sp + "_" + bw + "_" + bp);
    features.push_back("swbw_" + sw + "_" + bw);
    features.push_back("spbp_" + sp + "_"+ bp);
  }
  if (buffNode != -1 && buffNode_1 != -1) {
    features.push_back("bpb1p_" + bp + "_" + bp1);
  }
  // three
  if (buffNode != -1 && buffNode_1 != -1 && buffNode_2 != -1) {
    features.push_back("bpb1pb2p_" + bp + "_" + bp1 + "_" + bp2);
  }
  if (stckNode != -1 && buffNode != -1 && buffNode_1 != -1) {
    features.push_back("spbpb1p_" + sp + "_" + bp + "_" + bp1);
  }
  if (head != -1 && stckNode != -1 && buffNode != -1) {
    features.push_back("shpspbp_" + shp + "_" + sp + "_" + bp);
  }
  if (stckNode != -1 && left != -1 && buffNode != -1) {
    features.push_back("spslpbp_" + sp + "_" + slp + "_" + bp);
  }
  if (stckNode != -1 && right != -1 && buffNode != -1) {
    features.push_back("spsrpbp_" + sp + "_" + srp + "_" + bp); 
  }
  

  features.push_back("bias");
  // add to feature map 
  for (auto& feature : features) {
    if (featureMap.find(feature) == featureMap.end()) {
      featureMap[feature] = featureMap.size() - 1;
    }
    x[featureMap[feature]] = 1;
  }
}


void shift() {
  stck[stckHead++] = buff[--buffHead];
}

void left() {
  --stckHead;
}

void right() {
  buff[buffHead - 1] = stck[--stckHead];
}

void addExamples(Graph* graph, vector<Example> *data) {
  stckHead = 0;
  buffHead = 0;
  for (int i = graph->nodes.size() - 1; i >= 0; i--) {
    buff[buffHead++] = i;
  }

  memset(outDeg, 0, sizeof(outDeg));
  for (auto& edge : graph->edges) {
    ++outDeg[edge.first]; 
  }
  shift();
  while (buffHead) {    
    SparseIntVec x;
    extractFeatures(graph, x);

    int y;
    if (!stckHead) {
      y = 0; 
      shift();
    } else {  
      int stckNode = stck[stckHead - 1];
      int buffNode = buff[buffHead - 1];
      // check if there is a LEFT edge
      if (graph->hasEdge(buffNode, stckNode)) {
        y = 1;
        left();
        --outDeg[buffNode];
      } else {
        // check if there is a RIGHT edge
        if (graph->hasEdge(stckNode, buffNode)) {
          if (outDeg[buffNode] == 0) {
            // right 
            y = 2;
            right();
            --outDeg[stckNode];
          } else {
            // if there is more right edges from buffNode, shift!
            y = 0;
            shift();
          }
        } else {
          // if there is no edge between stckNode and buffNode, shift!
          y = 0;
          shift();
        }
      }
    }
    data->push_back(make_pair(x, y));
  }
}

void predictTree(Perceptron* model, Graph* graph, vector<Edge> *edges) {
  stckHead = 0;
  buffHead = 0;
  for (int i = graph->nodes.size() - 1; i >= 0; i--) {
    buff[buffHead++] = i;
  }
  
  shift();
  while (buffHead) { 
    
    if (!stckHead) {
       shift();
       continue;
    }
    SparseIntVec x;
    extractFeatures(graph, x);
    int y = model->predict(x);

    int stckNode = stck[stckHead - 1];
    int buffNode = buff[buffHead - 1];
    switch (y) {
      case 0: shift(); break;
      case 1: left(); edges->push_back(make_pair(buffNode, stckNode)); break;
      case 2: right(); edges->push_back(make_pair(stckNode, buffNode)); break; 
      default: break;
    }
  }
}

void evaluate(Perceptron* model, vector<Graph*> &testGraphs, float &acc) {
  vector<Edge> edges;
  int totalCorrect = 0;
  int totalPred = 0;
  for (auto& graph : testGraphs) {
    totalPred += (int)graph->nodes.size() - 1;
    edges.clear();
    predictTree(model, graph, &edges);
    for (auto& edge : edges) {
      if (graph->hasEdge(edge.first, edge.second)) {
        ++totalCorrect; 
      }
    }
  }
  acc = totalCorrect * 100. / totalPred;
}

/*void test() {
  vector<Graph*> trainGoldGraphs, trainPredGraphs;
  readData("tmp", &trainGoldGraphs, &trainPredGraphs);
  vector< Example > trainGold, trainPred;
  
  addExamples(trainGoldGraphs[0], &trainGold);
  addExamples(trainPredGraphs[0], &trainPred);
     
  for (auto& feature : featureMap) {
    cout << feature.first << ' ' << feature.second << endl;
  }

  int numClasses = 3;
  Perceptron model(numClasses);
  float alpha = 0;
  model.trainNoisy(trainGold, trainPred, alpha);
  
  vector<Graph*> testGoldGraphs, testPredGraphs;
  readData("data.dev", &testGoldGraphs, &testPredGraphs);
  vector<Edge> edges;
  predictTree(&model, testGoldGraphs[0], &edges);
  for (auto& edge : edges) {
    cout << edge.first << ' ' << edge.second << endl;
  }

}*/

void gridSearch(vector<Example> &trainGold, vector<Example> &trainPred, 
            vector<Graph*> goldGraphs, vector<Graph*> predGraphs, int seed, float &bestAlpha) {
  bestAlpha = -1;
  float bestAcc= -1;
  cout << setw(20) << "Alpha" << setw(20) << "Acc Pred" << setw(20) 
       << "Acc Gold" << setw(20) << "Acc Train" << setw(20) << "Diff" << endl;
  for (float alpha = 0; alpha <= 1.01; alpha += 0.1) {
    Perceptron model(NUMCLASSES);
    model.trainNoisy(trainGold, trainPred, alpha, seed);
    
    float accGold;
    evaluate(&model, goldGraphs, accGold);

    float accPred;
    evaluate(&model, predGraphs, accPred);
    
    float accTrain = alpha * accPred + (1 - alpha) * accGold; 
    cout << setw(20) << alpha << setw(20) << accPred << setw(20) 
         << accGold << setw(20) << accTrain << setw(20) << accPred - accTrain << endl;

    if (accPred > bestAcc) {
      bestAcc = accPred;
      bestAlpha = alpha;
    }
  }
}

void exp1(int seed) {
  readData("../data/data.tr", &trainGoldGraphs, &trainPredGraphs);

  for (auto& graph : trainGoldGraphs) {
    addExamples(graph, &trainGold);
  }
  for (auto& graph : trainPredGraphs) {
    addExamples(graph, &trainPred);
  }

  Perceptron model(NUMCLASSES);
  model.trainNoisy(trainGold, trainPred, seed, 0);

  for (int i = 1000; i <= 8000; i += 1000) {
  
    string devfile = "../data/data.dev." + to_string(i);
    if (i > 7000) devfile = "../data/data.dev";
    devGoldGraphs.clear();
    devPredGraphs.clear();
    readData(devfile, &devGoldGraphs, &devPredGraphs);

    string testfile = "../data/data.te." + to_string(i);
    if (i > 7000) testfile = "../data/data.te";
    testGoldGraphs.clear();
    testPredGraphs.clear();
    readData(testfile, &testGoldGraphs, &testPredGraphs);

    float acc;

    evaluate(&model, devPredGraphs, acc);
    cout << acc << '\t';
    evaluate(&model, testPredGraphs, acc);
    cout << acc << endl;
  }
}

void exp2(int seed) {
  
  readData("../data/data.tr", &trainGoldGraphs, &trainPredGraphs);
  cout << "Done reading train data: " << trainGoldGraphs.size() << " sentences " <<  endl;

  for (auto& graph : trainGoldGraphs) {
    addExamples(graph, &trainGold);
  }
  for (auto& graph : trainPredGraphs) {
    addExamples(graph, &trainPred);
  }
  cout << "Done extracting features" << endl;
  
  readData("../data/data.dev", &devGoldGraphs, &devPredGraphs);
  cout << "Done reading test data " << devGoldGraphs.size() << " sentences" << endl;

  readData("../data/data.te", &testGoldGraphs, &testPredGraphs);
  cout << "Done reading test data " << testGoldGraphs.size() << " sentences" << endl;


  float bestAlpha;
  cout << "PERFORMANCE ON DEV" << endl;
  gridSearch(trainGold, trainPred, devGoldGraphs, devPredGraphs, seed, bestAlpha);
  cout << "Best alpha = " << bestAlpha << endl;

  cout << "PERFORMANCE ON TEST" << endl;
  gridSearch(trainGold, trainPred, testGoldGraphs, testPredGraphs, seed, bestAlpha);
}

void exp3(int seed) {
  readData("../data/data.tr", &trainGoldGraphs, &trainPredGraphs);
  for (auto& graph : trainGoldGraphs) {
    addExamples(graph, &trainGold);
  }
  for (auto& graph : trainPredGraphs) {
    addExamples(graph, &trainPred);
  }

  for (int i = 1000; i <= 8000; i += 1000) { 
    string devfile = "../data/data.dev." + to_string(i);
    if (i > 7000) devfile = "../data/data.dev";
    devGoldGraphs.clear();
    devPredGraphs.clear();
    readData(devfile, &devGoldGraphs, &devPredGraphs);

    string testfile = "../data/data.te." + to_string(i);
    if (i > 7000) testfile = "../data/data.te";
    testGoldGraphs.clear();
    testPredGraphs.clear();
    readData(testfile, &testGoldGraphs, &testPredGraphs);

    cout << "POS size = " << i << endl;

    float bestAlpha;
    gridSearch(trainGold, trainPred, devGoldGraphs, devPredGraphs, seed, bestAlpha);
    cout << "Best alpha = " << bestAlpha << endl;
    
    cout << setw(20) << "Alpha opt" << setw(20) << "Alpha 0" << setw(20) << "Alpha 1" << endl;
   
    float acc;

    Perceptron modelOpt(NUMCLASSES);
    modelOpt.trainNoisy(trainGold, trainPred, bestAlpha, seed);
    evaluate(&modelOpt, testPredGraphs, acc);
    cout << setw(20) << acc;

    Perceptron model0(NUMCLASSES);
    model0.trainNoisy(trainGold, trainPred, 0, seed);
    evaluate(&model0, testPredGraphs, acc);
    cout << setw(20) << acc;

    Perceptron model1(NUMCLASSES);
    model1.trainNoisy(trainGold, trainPred, 1, seed);
    evaluate(&model1, testPredGraphs, acc);
    cout << setw(20) << acc << endl;
  }

}

void exp4(int seed) {
  readData("../data/data.tr", &trainGoldGraphs, &trainPredGraphs);
  cout << "Done reading train data: " << trainGoldGraphs.size() << " sentences " <<  endl;

  for (auto& graph : trainGoldGraphs) {
    addExamples(graph, &trainGold);
  }
  for (auto& graph : trainPredGraphs) {
    addExamples(graph, &trainPred);
  }
  cout << "Done extracting features" << endl;
  
  readData("../data/data.dev", &devGoldGraphs, &devPredGraphs);
  cout << "Done reading test data " << devGoldGraphs.size() << " sentences" << endl;

  readData("../data/data.te", &testGoldGraphs, &testPredGraphs);
  cout << "Done reading test data " << testGoldGraphs.size() << " sentences" << endl;


  Perceptron model(NUMCLASSES);
  model.MAX_ITER = 1;
  float alpha = 1;
  vector<int> indices;
  for (int i = 0; i < (int)trainGold.size(); i++) {
    indices.push_back(i);
  }
  srand(seed);
   cout << setw(30) << "PERFORMANCE ON DEV" << setw(30) << "PERFORMANCE ON TEST" << endl;

  for (int iter = 0; iter < 10; iter++) {
    random_shuffle(indices.begin(), indices.end());
    model.iterate(trainGold, trainPred, indices, alpha);
    alpha *= 0.9;

    model.average();

    float acc;
    evaluate(&model, devPredGraphs, acc);
    cout << alpha << setw(20) << acc;
    evaluate(&model, testPredGraphs, acc);
    cout << setw(20) << acc << endl;

    model.unaverage();
  }
}

int main(int argc, char* argv[]) {
  int seed = 123;
  if (argc > 1) {
    seed = atoi(argv[1]);
  }
  
  exp4(seed);
}
