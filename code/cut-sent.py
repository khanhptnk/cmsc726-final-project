from nltk.tokenize import blankline_tokenize
import sys
filename, numLines = sys.argv[1:]
sents = blankline_tokenize(open(filename, "r").read())[:int(numLines)]

for sent in sents:
    print sent
    print
